﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.AspNetCore.Components.Routing;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Text;

namespace BlazorApp1.Shared
{
    public class GenericComponent<T> : ComponentBase
    {
        protected T Component; //Company

        [Parameter]
        public Mode RenderMode { get; set; }

        private string Types {  get; set; }

        [Parameter]
        public RenderFragment ToolBar { get; set; }

        public GenericComponent()
        {
            var sb = new StringBuilder();
            foreach (var p in typeof(T).GetProperties())
            {
                sb.AppendLine(p.Name);
                sb.AppendLine(p.GetDisplayName<T>())
            }

            //Types = sb.ToString();
        }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            int i = 0;
            

            Component.RenderComponent<T>();

            builder.AddContent(0, $"{Types}");
            builder.AddContent(0, $"{RenderMode}");

            //builder.AddMarkupContent(0, RenderLabel("name","Bootproject"));


            base.BuildRenderTree(builder);
        }

        //private string RenderLabel(string name, string value)
        //{
        //    string component = string.Empty;
        //    switch (RenderMode) 
        //    {
        //        case Mode.Display:
        //            component = $"<label>{name}</label>";
        //            break;

        //        case Mode.Edit:
        //            component = $"<input type=\"text\" name=\"{name}\" value=\"{value}\" />";
        //            break;
        //    }

        //    return component;
        //}
    }


    public static class ModelExtensions
    {
        public static string GetDisplayName<T>(this Expression<Func<T>> expr)
        {
            var expression = (MemberExpression)expr.Body;
            var value = expression.Member.GetCustomAttributes(typeof(UIHintAttribute), true);
            return value.Length > 0 ? expression.Member.Name : "";
        }
    }

    public static class ComponentRenderService
    {
        public static RenderFragment RenderComponent<T>(this T model)
        {
            return BuildRenderTree =>
            {
                BuildRenderTree.OpenComponent(0, typeof(T));
                BuildRenderTree.AddAttribute(1, "Model", model);
                BuildRenderTree.CloseComponent();
            };
        }

    }
}
