﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Shared
{
    public partial class Organisation : ComponentBase
    {
        [Parameter]
        public Address Address { get; set; }

        [Parameter]
        public RenderFragment ToolBar { get; set; }

    }
}

