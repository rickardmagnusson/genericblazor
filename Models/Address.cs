﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlazorApp1
{
    
    public class Address
    {
        [DisplayName("Navn")]
        public string Name { get; set; }

        [DisplayName("Gate")]
        public string StreetAddress { get; set; }

        [DisplayName("Land")]
        public string AddressLocality { get; set; }

        [DisplayName("Adresse")]
        public string AddressRegion { get; set; }
     
        [DisplayName("Post.nr")]
        [UIHint("Number")]
        public string PostalCode { get; set; }

    }

    public class Company
    {
        [UIHint("Address")]
        public Address Address {  get; set; }
    }

}
