﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Pages
{
    public partial class Index
    {
        private Company Company {  get; set; }

        public Index()
        {
            Company = new Company
            {
                Address = new()
                {
                    Name = "WLCOM AS",
                    StreetAddress = "Wlcom gate 24",
                    AddressLocality = "NORWAY",
                    AddressRegion = "Jessheim",
                    PostalCode = "9801"
                }
            };
        }
    }
}
